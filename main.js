var CryptoJS = require("crypto-js");

Parse.Cloud.define('fundLoan', function(request, response) {         
  
  var amountToFund = request.params.amountToFund;
  var loanObjectIdToBeFunded = request.params.loanObjectIdToBeFunded;

  var successResponse = 'fund_success';
  var errorResponse = 'fund_fail';

  var base_url = 'https://sandboxapi.rapyd.net';
  var access_key = 'CE7C4C58CFD27B2342EA';
  var secret_key = '6d86d6547fe59531e9c18441b1f5fe4fe1f155b6db0d1c35919e4d52d1f66c752f7124b48873fbce';

  var idempotency = new Date().getTime().toString();
  var http_method = 'post';
  var url_path = '/v1/checkout';
  var salt = CryptoJS.lib.WordArray.random(12);
  var timestamp = (Math.floor(new Date().getTime() / 1000) - 10).toString();

  var requestData = {
                      "amount": parseFloat(amountToFund),
                      "complete_payment_url": "https://app.worldlend.co/lender/checkout-fund-success.html",
                      "country": "MX",
                      "currency": "MXN",
                      "error_payment_url": "https://app.worldlend.co/lender/checkout-fund-error.html",
                      "merchant_reference_id": "950ae8c6-78",
                      "cardholder_preferred_currency": true,
                      "language": "en",
                      "metadata": {
                          "merchant_defined": true
                      },
                      "payment_method_types_include": [
                          "mx_bbva_bank"
                      ],
                      "payment_method_types_exclude": []
                    };

  var body = '';

  if (JSON.stringify(requestData) !== '{}' && requestData !== '') {
    body = JSON.stringify(requestData);
  }

  let to_sign = http_method + url_path + salt + timestamp + access_key + secret_key + body;

  var signature = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(to_sign, secret_key));

  signature = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(signature));

  console.log(signature);

  Parse.Cloud.httpRequest({
    method: 'POST',
    url: 'https://sandboxapi.rapyd.net/v1/checkout',
    headers: {
      'Content-Type': 'application/json',
      'salt': salt,
      'timestamp': timestamp,
      'signature': signature,
      'access_key': access_key,
      'idempotency': idempotency
    },
    body: requestData
    }).then(function(httpResponse) {
        
        // success
        console.log('success:' + httpResponse);

        var responseObject = {message:successResponse,httpResponse:httpResponse};
        response.success(responseObject);

    }, function(httpResponse) {
        // error
        console.error('Request failed with response code ' + httpResponse.status);
        var responseObject = {message:errorResponse,httpResponse:httpResponse};
        response.error(responseObject);
    });

});

Parse.Cloud.define('payLoan', function(request, response) {         
  
  var amountToPay = request.params.amountToPay;
  var loanObjectIdToBePaid = request.params.loanObjectIdToBePaid;

  var successResponse = 'fund_success';
  var errorResponse = 'fund_fail';

  var base_url = 'https://sandboxapi.rapyd.net';
  var access_key = 'CE7C4C58CFD27B2342EA';
  var secret_key = '6d86d6547fe59531e9c18441b1f5fe4fe1f155b6db0d1c35919e4d52d1f66c752f7124b48873fbce';

  var idempotency = new Date().getTime().toString();
  var http_method = 'post';
  var url_path = '/v1/checkout';
  var salt = CryptoJS.lib.WordArray.random(12);
  var timestamp = (Math.floor(new Date().getTime() / 1000) - 10).toString();

  var requestData = {
                      "amount": parseFloat(amountToPay),
                      "complete_payment_url": "https://app.worldlend.co/borrower/checkout-pay-success.html",
                      "country": "MX",
                      "currency": "MXN",
                      "error_payment_url": "https://app.worldlend.co/borrower/checkout-pay-error.html",
                      "merchant_reference_id": "950ae8c6-78",
                      "cardholder_preferred_currency": true,
                      "language": "en",
                      "metadata": {
                          "merchant_defined": true
                      },
                      "payment_method_types_include": [
                          "mx_bbva_bank"
                      ],
                      "payment_method_types_exclude": []
                    };

  var body = '';

  if (JSON.stringify(requestData) !== '{}' && requestData !== '') {
    body = JSON.stringify(requestData);
  }

  let to_sign = http_method + url_path + salt + timestamp + access_key + secret_key + body;

  var signature = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(to_sign, secret_key));

  signature = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(signature));

  console.log(signature);

  Parse.Cloud.httpRequest({
    method: 'POST',
    url: 'https://sandboxapi.rapyd.net/v1/checkout',
    headers: {
      'Content-Type': 'application/json',
      'salt': salt,
      'timestamp': timestamp,
      'signature': signature,
      'access_key': access_key,
      'idempotency': idempotency
    },
    body: requestData
    }).then(function(httpResponse) {
        
        // success
        console.log('success:' + httpResponse);

        var responseObject = {message:successResponse,httpResponse:httpResponse};
        response.success(responseObject);

    }, function(httpResponse) {
        // error
        console.error('Request failed with response code ' + httpResponse.status);
        var responseObject = {message:errorResponse,httpResponse:httpResponse};
        response.error(responseObject);
    });

});

// loan funding is complete -> disburse money to borrower

Parse.Cloud.define('payoutToBorrower', function(request, response) {         
  
  var amountPayoutToBorrower = request.params.amountPayoutToBorrower;
  var loanObjectId = request.params.loanObjectId;
  var loanOwner = request.params.loanOwner;

  var successResponse = 'fund_success';
  var errorResponse = 'fund_fail';

  var base_url = 'https://sandboxapi.rapyd.net';
  var access_key = 'CE7C4C58CFD27B2342EA';
  var secret_key = '6d86d6547fe59531e9c18441b1f5fe4fe1f155b6db0d1c35919e4d52d1f66c752f7124b48873fbce';

  var idempotency = new Date().getTime().toString();
  var http_method = 'post';
  var url_path = '/v1/payouts';
  var salt = CryptoJS.lib.WordArray.random(12);
  var timestamp = (Math.floor(new Date().getTime() / 1000) - 10).toString();

  var requestData = {
                      "ewallet": "ewallet_b6625ab68b4a4f37d97c04c6e2df1e9f",
                      "payout_amount": parseFloat(amountPayoutToBorrower),
                      "confirm_automatically": false,
                      "payout_method_type": "mx_chedraui_cash",
                      "sender_currency": "USD",
                      "sender_country": "MX",
                      "beneficiary_country": "MX",
                      "payout_currency": "MXN",
                      "sender_entity_type": "company",
                      "beneficiary_entity_type": "individual",
                      "beneficiary": {
                          "payment_type": "regular",
                          "address": loanOwner.get('borrowerOnboarding').form.address.street,
                          "city": loanOwner.get('borrowerOnboarding').form.address.city,
                          "country": loanOwner.get('borrowerOnboarding').form.address.countryCode,
                          "first_name": loanOwner.get('name'),
                          "last_name": loanOwner.get('firstLastname'),
                          "state": loanOwner.get('borrowerOnboarding').form.address.state,
                          "phone_number": loanOwner.get('phone')
                      },
                      "sender": {
                          "company_name": "WorldLend.co",
                          "city": "Miguel Hidalgo",
                          "state": "CDMX",
                          "phone_number": "+5215528042599"
                      },
                      "description": '"Deposit for loan id' + loanObjectId + '"'
                  };

  var body = '';

  if (JSON.stringify(requestData) !== '{}' && requestData !== '') {
    body = JSON.stringify(requestData);
  }

  let to_sign = http_method + url_path + salt + timestamp + access_key + secret_key + body;

  var signature = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(to_sign, secret_key));

  signature = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(signature));

  Parse.Cloud.httpRequest({
    method: 'POST',
    url: 'https://sandboxapi.rapyd.net/v1/checkout',
    headers: {
      'Content-Type': 'application/json',
      'salt': salt,
      'timestamp': timestamp,
      'signature': signature,
      'access_key': access_key,
      'idempotency': idempotency
    },
    body: requestData
    }).then(function(httpResponse) {
        
        // success
        console.log('success:' + httpResponse);

        var responseObject = {message:successResponse,httpResponse:httpResponse};
        response.success(responseObject);

    }, function(httpResponse) {
        // error
        console.error('Request failed with response code ' + httpResponse.status);
        var responseObject = {message:errorResponse,httpResponse:httpResponse};
        response.error(responseObject);
    });

});

// borrowe made a monthly payment -> make disbursement to all loan investors including earnings

Parse.Cloud.define('payoutToLender', function(request, response) {         
  
  var amountPayoutToLender = request.params.amountPayoutToLender;
  var loanObjectId = request.params.loanObjectId;
  var loanFunding = request.params.loanFunding;

  var successResponse = 'fund_success';
  var errorResponse = 'fund_fail';

  var base_url = 'https://sandboxapi.rapyd.net';
  var access_key = 'CE7C4C58CFD27B2342EA';
  var secret_key = '6d86d6547fe59531e9c18441b1f5fe4fe1f155b6db0d1c35919e4d52d1f66c752f7124b48873fbce';

  var idempotency = new Date().getTime().toString();
  var http_method = 'post';
  var url_path = '/v1/payouts';
  var salt = CryptoJS.lib.WordArray.random(12);
  var timestamp = (Math.floor(new Date().getTime() / 1000) - 10).toString();

  var requestData = {
                      "ewallet": "ewallet_b6625ab68b4a4f37d97c04c6e2df1e9f",
                      "payout_amount": parseFloat(amountPayoutToLender),
                      "confirm_automatically": false,
                      "payout_method_type": "mx_chedraui_cash",
                      "sender_currency": "USD",
                      "sender_country": "MX",
                      "beneficiary_country": "MX",
                      "payout_currency": "MXN",
                      "sender_entity_type": "company",
                      "beneficiary_entity_type": "individual",
                      "beneficiary": {
                          "payment_type": "regular",
                          "address": loanFunding.get('owner').get('borrowerOnboarding').form.address.street,
                          "city": loanFunding.get('owner').get('borrowerOnboarding').form.address.city,
                          "country": loanFunding.get('owner').get('borrowerOnboarding').form.address.countryCode,
                          "first_name": loanFunding.get('owner').get('name'),
                          "last_name": loanFunding.get('owner').get('firstLastname'),
                          "state": loanFunding.get('owner').get('borrowerOnboarding').form.address.state,
                          "phone_number": loanFunding.get('owner').get('phone')
                      },
                      "sender": {
                          "company_name": "WorldLend.co",
                          "city": "Miguel Hidalgo",
                          "state": "CDMX",
                          "phone_number": "+5215528042599"
                      },
                      "description": '"Deposit for loan id' + loanObjectId + '"'
                  };

  var body = '';

  if (JSON.stringify(requestData) !== '{}' && requestData !== '') {
    body = JSON.stringify(requestData);
  }

  let to_sign = http_method + url_path + salt + timestamp + access_key + secret_key + body;

  var signature = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(to_sign, secret_key));

  signature = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(signature));

  Parse.Cloud.httpRequest({
    method: 'POST',
    url: 'https://sandboxapi.rapyd.net/v1/checkout',
    headers: {
      'Content-Type': 'application/json',
      'salt': salt,
      'timestamp': timestamp,
      'signature': signature,
      'access_key': access_key,
      'idempotency': idempotency
    },
    body: requestData
    }).then(function(httpResponse) {
        
        // success
        console.log('success:' + httpResponse);

        var responseObject = {message:successResponse,httpResponse:httpResponse};
        response.success(responseObject);

    }, function(httpResponse) {
        // error
        console.error('Request failed with response code ' + httpResponse.status);
        var responseObject = {message:errorResponse,httpResponse:httpResponse};
        response.error(responseObject);
    });

});